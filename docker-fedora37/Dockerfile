FROM fedora:37

MAINTAINER Daiki Ueno <ueno@gnu.org>

ARG dnfflags="--allowerasing"

RUN dnf install $dnfflags -y make patch ccache git which autoconf libtool gettext-devel automake autogen nettle-devel libtpms-devel p11-kit-devel \
	autogen-libopts-devel guile22-devel libidn2-devel gawk gperf libtasn1-devel libtasn1-tools unbound-devel bison help2man xz net-tools rsync wget \
	libseccomp-devel libasan libtsan libasan-static libubsan libubsan-static nodejs datefudge lcov gcovr openssl-devel dieharder \
	openssl libcmocka-devel socat xz ppp libabigail valgrind libunistring-devel libatomic \
	expect softhsm libev-devel dash iproute \
	gtk-doc texinfo texinfo-tex texlive texlive-supertabular texlive-framed texlive-morefloats texlive-quotchap docbook5-style-xsl docbook-style-xsl \
	python3-flake8 python3-jsonschema python3-mypy python3-six \
	python3.6 tox \
	python-unversioned-command zip \
	clang compiler-rt clang-analyzer llvm cppcheck \
	libkcapi-devel fipscheck \
	nmap-ncat tpm-tools trousers-devel trousers swtpm \
	tpm2-tools tpm2-tss-devel tpm2-tss-engine-utilities \
	zlib-devel brotli-devel libzstd-devel \
	rubygem-rexml \
	beakerlib tmt && \
	dnf clean all

RUN chmod 755 $(find /usr/share/sgml/docbook/xsl-stylesheets-*/ -name dbtoepub -print)
RUN texconfig rehash

RUN wget http://deb.debian.org/debian/pool/main/p/pmccabe/pmccabe_2.6.tar.gz && tar xvf pmccabe_2.6.tar.gz && cd pmccabe && make && cp pmccabe /usr/local/bin && rm -rf pmccabe pmccabe_2.6.tar.gz

RUN mkdir -p /usr/local/ && git clone https://gitlab.com/libidn/gnulib-mirror.git /usr/local/gnulib
ENV GNULIB_SRCDIR /usr/local/gnulib
ENV GNULIB_TOOL /usr/local/gnulib/gnulib-tool
